import { Injectable } from '@angular/core';
import { HttpClient , HttpErrorResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Ibook } from './books';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  private _url : string = "http://localhost:8082/library/books";

  constructor(private http : HttpClient) { }

  getBooks() :Observable<Ibook[]>{
    return this.http.get<Ibook[]>(this._url).pipe(
      catchError(this.errorHandler));
  }

  deleteBook(book : Ibook) :Observable<Ibook>{
    return this.http.delete<Ibook>(this._url+"/"+book.id).pipe(
      catchError(this.errorHandler));
  }
 
  updateBook(book : Ibook) : Observable<Ibook>{
    return this.http.put<Ibook>(this._url+"/"+book.id , book).pipe(
      catchError(this.errorHandler));
  }

  createBook(book : Ibook) : Observable<Ibook>{
    return this.http.post<Ibook>(this._url,book).pipe(
      catchError(this.errorHandler));
  } 

  errorHandler(error : HttpErrorResponse){
    return Observable.throw(error.message);
  }

}
