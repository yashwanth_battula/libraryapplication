import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenResponse } from './TokenResponse';


@Injectable({
  providedIn: 'root'
}) 
export class AuthService {

  private _url : string = "http://localhost:8082/authenticate";

  constructor(private http : HttpClient , private router : Router) { }

  public jsonData : any;

  getAuthenticate(username : string , password : string ):Observable<TokenResponse>{
    this.jsonData = {
      "username" : username,
      "password" : password
    };
    console.log(this.jsonData);
    return this.http.post<any>(this._url,this.jsonData);
  }

  loggedIn(){
    return !!localStorage.getItem('token');
  }

  getToken(){
    return localStorage.getItem('token');
  }

  logoutUser(){
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }
  
}
