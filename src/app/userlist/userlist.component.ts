import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../users';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {

  constructor(private userService : UserService , private toastr : ToastrService , private authService : AuthService) { 
    this.selectedUser = new User();
  }

  isEditUser: Boolean=false;

  dialogTitle : string ;

  selectedUser : User;

  public users = [];

  public error;

  ngOnInit(): void {
    this.userService.getUsers().subscribe(data => this.users = data , error => this.toastr.error("Failed to fetch user list"));
  }

  delete(user : User){
    this.userService.deleteUser(user).subscribe(data => {
      console.log("user deleted");
      this.userService.getUsers().subscribe(data => this.users = data , error => this.toastr.error("Failed to remove user"));
    });
  }

  create(){
    console.log("create user");
    this.selectedUser = new User();
    this.isEditUser = false;
    this.dialogTitle = "Add User";
  }

  update(user : User){
    console.log("update User");
    this.selectedUser = user;
    this.isEditUser = true;
    this.dialogTitle = "Edit User";
  }

  saveUserAck(event){
    document.getElementById("closePopup").click();
    this.userService.getUsers().subscribe(data => this.users = data , error => this.toastr.error("Failed to fetch user list"));
  }

  logoutUser(){
    this.authService.logoutUser();
  }
  
}
