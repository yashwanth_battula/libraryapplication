import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BooklistComponent } from './booklist/booklist.component';
import { UserlistComponent } from './userlist/userlist.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CirculationComponent } from './circulation/circulation.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './auth.guard';


const routes: Routes = [
  {path : '' , redirectTo : '/login' , pathMatch : 'full'},
  {path : 'books' , component : BooklistComponent , canActivate : [AuthGuard]},
  {path : 'users' , component : UserlistComponent , canActivate : [AuthGuard]},
  {path : 'circulation' , component : CirculationComponent , canActivate : [AuthGuard]},
  {path : 'login' , component : LoginComponent},
  {path : 'home' , component : HomeComponent , canActivate : [AuthGuard]},
  {path : "**" , component : PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingcomp = [BooklistComponent , UserlistComponent , CirculationComponent , PageNotFoundComponent , LoginComponent , HomeComponent] 
