import { User } from './users';
import { Ibook } from './books';

export class UserBook{
    public user : User;
    public bookList : Ibook[];
} 
