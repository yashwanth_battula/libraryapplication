import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router : Router , private toastr : ToastrService , private authService : AuthService) { }

  public username : string ;

  public password : string ;

  ngOnInit(): void {
  }

  getlogin(){
    this.authService.getAuthenticate(this.username , this.password).subscribe(data  => {
      console.log(data.jwt);
      localStorage.setItem('token' , data.jwt);
      console.log("valid creadentials");
      this.router.navigate(['/home']);
    } , error => {
      console.log("Invalid creadentials");
      this.toastr.error("Invalid creadentials");
    });
  } 

}
