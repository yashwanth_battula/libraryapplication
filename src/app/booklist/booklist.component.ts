import { Component, OnInit } from '@angular/core';
import { BookService } from '../book.service';
import {Ibook} from '../books';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-booklist',
  templateUrl: './booklist.component.html',
  styleUrls: ['./booklist.component.css']
})
export class BooklistComponent implements OnInit {

  constructor(private bookService : BookService , private toastr : ToastrService , private authService : AuthService) { 
    this.selectedBook = new Ibook();
  }

  isEdit: Boolean=false;

  dialogTitle : string ;

  selectedBook : Ibook;

  public books = [];

  public error;


  ngOnInit(): void {
    this.bookService.getBooks().subscribe(data => this.books = data ,
      error => this.toastr.error("Books could not be fetched"));
  }

  delete(book : Ibook){
    this.bookService.deleteBook(book).subscribe(data => {
      console.log("Book deleted");
      this.bookService.getBooks().subscribe(data => this.books = data);
    },
    error => this.toastr.error("Book cannot be deleted"));
  }

  create(){
    console.log("create book in BookList");
    this.selectedBook = new Ibook();
    this.isEdit = false;
    this.dialogTitle = "Add Book";
  }

  update(book : Ibook){
    console.log("update book in BookList");
    this.selectedBook = book;
    this.isEdit = true;
    this.dialogTitle = "Edit Book";
  }

  saveBookAck(event){
    document.getElementById("closePopup").click();
    this.bookService.getBooks().subscribe(data => this.books = data ,  error => this.toastr.error("Books could not be fetched"));
  }

  logoutUser(){
    this.authService.logoutUser();
  }

}
