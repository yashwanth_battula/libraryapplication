import { Component, OnInit } from '@angular/core';
import { UserBook } from '../userBook';
import { UserBookService } from '../user-book.service';
import { Ibook } from '../books';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-circulation',
  templateUrl: './circulation.component.html',
  styleUrls: ['./circulation.component.css']
})
export class CirculationComponent implements OnInit {

  constructor(private userBookService : UserBookService , private toastr : ToastrService , private authService : AuthService) { }

  public userId = 0;

  public bookId = 0;

  public userBook : UserBook;
 
  IsUserIdSubmited : Boolean = false; 

  public error;
 
  ngOnInit(): void {
  }

  getUserInfo(){
   console.log("getting user and book information of "+this.userId);
   this.userBookService.getUserBook(this.userId).subscribe(data => this.userBook = data , error => this.toastr.error("user and book information of "+this.userId+" cannot be fetched"));
   this.IsUserIdSubmited = true;
  }

  create(){
    console.log("adding book to the user");
    this.userBookService.addBookForUser(this.userId , this.bookId).subscribe(data =>{
      document.getElementById("closePopup").click();
      this.userBookService.getUserBook(this.userId).subscribe(data => this.userBook = data , error => this.toastr.error("user and book information of "+this.userId+" cannot be fetched"));
      this.IsUserIdSubmited = true;
    }, error => this.toastr.error("Book can not be added to the user"));
  }

  delete(book : Ibook){
    console.log("Releasing book for user");
    this.userBookService.deleteBookForUser(this.userId , book.id).subscribe(data => {
      this.userBookService.getUserBook(this.userId).subscribe(data => this.userBook = data , error => this.toastr.error("user and book information of "+this.userId+" cannot be fetched"));
      this.IsUserIdSubmited = true;
    }, error => this.toastr.error("Book can not be deleted to the user"));
  }

  logoutUser(){
    this.authService.logoutUser();
  }

}
