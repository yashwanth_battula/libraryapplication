import { Component, OnInit, Input , EventEmitter ,Output } from '@angular/core';
import { BookService } from '../book.service';
import {Ibook} from '../books';
import { error } from 'protractor';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  @Input()
  isEdit : boolean = false;

  @Input()
  book : Ibook;

  @Output()
  saveEvent = new EventEmitter<any>();

  books : Ibook[];

  constructor(private bookService : BookService , private toastr : ToastrService) { }

  ngOnInit(): void {
    this.bookService.getBooks().subscribe(data => this.books = data ,
      error => this.toastr.error("Books could not be fetched"));
  }

  saveBook():void{
      if(this.isEdit == true){
        this.bookService.updateBook(this.book).subscribe(data => {
          console.log("book is updated");
          this.saveEvent.emit("success");
          this.bookService.getBooks();
        },
        error => this.toastr.error("Book can not be updated"));
      }else{
        this.bookService.createBook(this.book).subscribe(data => {
          console.log("book is updated");
          this.saveEvent.emit("success");
          this.bookService.getBooks();
        },
        error => this.toastr.error("Book cannot be created"));
      }
  }

}
