import { Injectable , Injector} from '@angular/core';
import { HttpInterceptor } from '@angular/common/http';
import { AuthService } from './auth.service';


@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor{

  constructor(private injector : Injector) { }

  intercept(req , next){
    let authService = this.injector.get(AuthService);
    let tokenizedrequest = "";
    if(authService.getToken() == null){
      tokenizedrequest = req.clone({
        headers: req.headers.set('Authorization', " ")
      })
    }else{
      tokenizedrequest = req.clone({
        headers: req.headers.set('Authorization', 'Bearer ' + authService.getToken())
      })
    }
    return next.handle(tokenizedrequest)
  }
}
