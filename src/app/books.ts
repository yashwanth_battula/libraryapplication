export class Ibook{
   public id : number;
   public title : string;
   public author : string;
   public genre : string;
   public price : number;
}