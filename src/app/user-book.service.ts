import { Injectable } from '@angular/core';
import { HttpClient , HttpErrorResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserBook } from './userBook';
import { Ibook } from './books';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserBookService {

  constructor( private http : HttpClient) { }

  private _url : string = "http://localhost:8082/library/users";

  getUserBook(userId : number) : Observable<UserBook>{
    return this.http.get<UserBook>(this._url+"/"+userId).pipe(
      catchError(this.errorHandler));
  }

  deleteBookForUser(userId :number , bookId : number) :Observable<UserBook>{
    return this.http.delete<UserBook>(this._url+"/"+userId+"/books/"+bookId).pipe(
      catchError(this.errorHandler));
  }

  addBookForUser(userId :number , bookId : number) : Observable<UserBook>{
    return this.http.post<UserBook>(this._url+"/"+userId+"/books/"+bookId , new Ibook).pipe(
      catchError(this.errorHandler));
  } 

  errorHandler(error : HttpErrorResponse){
    return Observable.throw(error.message);   
  }


}
