import { Component, OnInit , Input , EventEmitter ,Output} from '@angular/core';
import { User } from '../users';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  @Input()
  isEditUser : boolean = false;

  @Input()
  user : User;

  @Output()
  saveEvent = new EventEmitter<any>();

  users : User[];

  public error ;

  constructor(private userService : UserService , private toastr : ToastrService) { }

  ngOnInit(): void {
    this.userService.getUsers().subscribe(data => this.users = data , error => this.toastr.error("Failed to fetch user list"));
  }

  saveUser():void{
      if(this.isEditUser == true){
        this.userService.updateUser(this.user).subscribe(data => {
          console.log("user is updated");
          this.saveEvent.emit("success");
          this.userService.getUsers();
        }, error => this.toastr.error("Failed to update user information"));
      }else{
        this.userService.createUser(this.user).subscribe(data => {
          console.log("user is created");
          this.saveEvent.emit("success");
          this.userService.getUsers();
        }, error => this.toastr.error("Failed to create user"));
      }
  }
}
