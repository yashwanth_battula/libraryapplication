import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from './users';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor( private http : HttpClient) { }

  private _url : string = "http://localhost:8082/library/users";

  getUsers() :Observable<User[]>{
    return this.http.get<User[]>(this._url).pipe(
      catchError(this.errorHandler));
  }

  deleteUser(user : User) :Observable<User>{
    return this.http.delete<User>(this._url+"/"+user.id).pipe(
      catchError(this.errorHandler));
  }

  updateUser(user : User) : Observable<User>{
    return this.http.put<User>(this._url+"/"+user.id , user).pipe(
      catchError(this.errorHandler));
  }

  createUser(user : User) : Observable<User>{
    return this.http.post<User>(this._url , user);
  } 

  errorHandler(error : HttpErrorResponse){
    return Observable.throw(error.message);
  }

}
